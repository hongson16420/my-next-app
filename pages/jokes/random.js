import { getRandomJoke } from "../../lib/joke"
import Card from "react-bootstrap/Card"
import Button from "react-bootstrap/Button"
import Layout from "../../components/Layout"
import Link from "next/link"

const Random = ({ joke }) => {
    return (
        <Layout>
            <Card className="my-3 shadow">
                <Card.Body>
                    <Card.Title>Here your random joke for today</Card.Title>
                    <Card.Text>{joke.value}</Card.Text>
                    <Link href="/posts">
                        <Button variant="dark">Back</Button>
                    </Link>
                </Card.Body>
            </Card>
        </Layout>
    )
}

export const getServerSideProps = async () => {
    const joke = await getRandomJoke() //let joke
    //joke = false 
    
    if(!joke) 
        return {
            notFound: true // 404 page
        }
        // return {
        //     redirect: {
        //         destination: '/posts',
        //         permanent: false
        //     }
        // }
    
    return {
        props: {
            joke
        }
    }
}

export default Random