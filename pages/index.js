import Layout from "../components/Layout"
import Link from 'next/link'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Button from 'react-bootstrap/Button'

export default function Home() {
  return (
    <Layout>
      <Jumbotron>
        <h1>My next app</h1>
        <p>
          This is my Next.js app
        </p>
        <p>
          <Link href="posts">
            <Button variant="primary">Learn More</Button>
          </Link>
        </p>
      </Jumbotron>
    </Layout>   
  )  
}
