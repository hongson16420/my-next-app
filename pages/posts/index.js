import Link from 'next/link'
import Card from 'react-bootstrap/Card'
import Layout from "../../components/Layout"
import {getPosts} from "../../lib/post"

const Posts = ({posts}) => {

    // const posts = [
    //     {
    //         id: 1, 
    //         title: 'Post 1',
    //         body: 'My post 1 body'
    //     },
    //     {
    //         id: 2, 
    //         title: 'Post 2',
    //         body: 'My post 2 body'
    //     },
    // ]

    return (
        <Layout>
            {posts.map(post => (
                <Card key={post.id}>
                    <Card.Body>
                        <Card.Title>{post.id} -- {post.title}</Card.Title>
                        <Card.Text>{post.body}</Card.Text>
                        <Link  href={`/posts/${post.id}`} passHref>
                            <Card.Link>See more...</Card.Link>
                        </Link>
                        
                    </Card.Body>
                </Card>
            ))}
        </Layout>   
    )
}

// Get static data from backend /db /api
/* getStaticProp dung khi mọi user request đều hiển thị ra nội dung như nhau, ai cung dung chung 1 data, 
còn khi mỗi user request nhưng trả về nội dung của mỗi user đó thì khác nhau thì ko dùng */
export const getStaticProps = async () => {
    const posts = await getPosts(10)
    return {
        props: {
            posts
        }
    }
}

export default Posts