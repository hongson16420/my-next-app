import Link from "next/link"
import Card from "react-bootstrap/Card"
import Button from "react-bootstrap/Button"
import Layout from "../../components/Layout"
import { getPostIds, getPostById } from "../../lib/post"
import { useRouter } from 'next/router'
import Spinner from 'react-bootstrap/Spinner'
import spinnerStyles from '../../styles/Spinner.module.css'

const Post = ({ post }) => {
    const router = useRouter()
    // Neu trang chua tạo ra, isFallback cua router === true
    // Va trang tam thoi sau day se duoc render

    if(router.isFallback) {
        return (
            <Spinner animation='border' role='status' variant='dark' className={spinnerStyles.spinnerLg}>
                <span className='sr-only'>Loading ....</span>
            </Spinner>
        )     
    }

    // Khi getStaticProps() chay xong lan dau tien
    // Cac lan sau, trang so 6(post 6) se duoc dua vao danh sach prerendered
    return <Layout>
        <Card className="my-3 shadow">
            <Card.Body>
                <Card.Title>{post.title}</Card.Title>
                <Card.Text>{post.body}</Card.Text>
                <Link href="/posts">
                    <Button variant="dark">Back</Button>
                </Link>
            </Card.Body>
        </Card>
    </Layout>
    
}
// lay du lieu kieu static, nhung du lieu tĩnh nao thi con phu thuoc vao path params
export const getStaticPaths = async () => {
    const paths = await getPostIds(5)
    console.log(paths)
    return {
        paths,
        fallback: true 
    //true: path(posts: 6 7 8 9 10) nào ko return ngay lap tuc se show trang tạm thời => đợi getStaticProps run -> return trang hoàn chinh
    //false: bat ki path nao ko returned boi getStaticPaths se toi trang 404
    }
}

export const getStaticProps = async ({params}) => {
    const post = await getPostById(params.id)
    return {
        props: {
            post
        },
        revalidate: 1 // nghia la khi nao ma database thay doi, tự gui di 1 request, tu cap nhat lai data cua trang. 
        // Dien ra nhieu nhat 1 lan trong 1 giay
    }
}

export default Post