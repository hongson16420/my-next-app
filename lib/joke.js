import axios from 'axios'
// du lieu phu thuoc vao mỗi request nhung van tạo ra html tĩnh cho front-end nen van tot cho SEO
export const getRandomJoke = async () => {
    try {
        const response = await axios.get('https://api.chucknorris.io/jokes/random')
        return response.data
    } catch (error) {
        console.log(error)
    }
}