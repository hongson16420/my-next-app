import fs from 'fs'
import path from 'path'

const booksDir = path.join(process.cwd(), 'books')

export const getBooks = () => {
    const booksFileNames = fs.readdirSync(booksDir)
    const booksData = booksFileNames.map(booksFileNames => {
        const fullBookPath = path.join(booksDir, booksFileNames)
        const bookContent = fs.readFileSync(fullBookPath, 'utf-8')
        return {
            bookName: booksFileNames.replace(/\.txt$/, ''),
            bookContent
        }
    })
    return booksData
}